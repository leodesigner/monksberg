//Entrada
$('#entrada').modal({backdrop: 'static', keyboard: false});
//Nav_fixed
$('.navbar-brand').hide();
$(window).on('scroll',function() {    
    var scrolltop = $(this).scrollTop();

    if(scrolltop >= 190) {
      $('.navbar-brand').show();
    }

    else if(scrolltop <= 190) {
     $('.navbar-brand').hide();
    }

    if(scrolltop >= 1200) {
        $(".animated").addClass("fadeInUp");
    }   

});

$('.collapse a').on('click', function(){
    $('.navbar-toggler').click();
});

$('a.scroll_client').on('click',function (e) {
    // e.preventDefault();

    var target = this.hash,
    $target = $(target);

   $('html, body').stop().animate({
     'scrollTop': $target.offset().top-10       
    }, 1500, 'swing', function () {
     window.location.hash = target;

    });

});

